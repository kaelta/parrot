import {Client, Events, GatewayIntentBits} from 'discord.js';

const client = new Client({intents: [GatewayIntentBits.Guilds]});

client.once(Events.ClientReady, c => {
    console.log('Ready~ <3');
});

client.login(process.env.DISCORD_TOKEN);

async function readUserFile(path) {
    const file = Bun.file(path)

    if (!file.exists) {
        throw new Error('File ' + file + 'does not exist!')
    }

    const contents = await file.json();

    return contents;
}

